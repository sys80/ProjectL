local B = CBW_Battle
local S = B.SkinVars

B.TailsCatchPlayer = function(player1,player2)
	if P_MobjFlip(player1.mo) != P_MobjFlip(player2.mo) then return end
	local flip = P_MobjFlip(player1.mo)
	//Determine who's who
	local tails,passenger
	if player1.mo.z*flip < player2.mo.z*flip
		tails = player2
		passenger = player1
	else
		tails = player1
		passenger = player2
	end
	
	//Get Z distance
	local zdist
	if flip == 1 then
		zdist = abs(tails.mo.z-(passenger.mo.z+passenger.mo.height))
	else
		zdist = abs(passenger.mo.z-(tails.mo.z+tails.mo.height))
	end
	
	//Apply gates
	if tails.panim != PA_ABILITY
	or not(tails.pflags&PF_CANCARRY)
	or zdist > FixedDiv(passenger.mo.height,passenger.mo.scale)/2
-- 	or passenger.carried_time < 15
	or passenger.powers[pw_carry]
	or passenger.mo.momz*flip > 0
	or passenger.exhaustmeter <= 0
	or passenger.powers[pw_tailsfly]
	or passenger.mo.state == S_PLAY_FLY_TIRED
		return
	end
	//Assign carry states
	B.DebugPrint("Latching "..passenger.name.." onto "..tails.name,DF_PLAYER)
	B.ResetPlayerProperties(passenger,false,false)
	passenger.mo.tracer = tails.mo
	passenger.powers[pw_carry] = CR_PLAYER
	passenger.carried_time = 0
-- 	tails.carry_id = passenger.mo
	S_StartSound(passenger.mo,sfx_s3k4a)
end

local homingthok = function(p)
	if p.homing and p.target and p.target.valid and p.target.player and p.target.player.valid and p.target.player.intangible
		p.homing = 0//Air dodging can "shake off" a player that is homing on to you
	else
		p.homing = min($,10)
	end
end

local popgun = function(player)
	if P_PlayerInPain(player) and player.charability2 == CA2_GUNSLINGER then
		player.weapondelay = TICRATE/3
	end
end

local pogo = function(player)
	local pmo = player.mo
	local momangle = R_PointToAngle2(0,0,player.rmomx,player.rmomy) //Used for angling new momentum in ability cases
	local pmom = FixedDiv(FixedHypot(player.rmomx,player.rmomy),pmo.scale) //Current speed, scaled for normalspeed calculations
	//Fang momentum renewal
	if player.charability == CA_BOUNCE then
		//Create bounce history
		if player.bouncelast == nil then
			player.bouncelast = false
		end
		if player.pflags&PF_BOUNCING and not(player.bouncelast) //Activate bounce
			or (not(player.pflags&PF_BOUNCING) and player.pflags&PF_THOKKED and player.bouncelast) //Deactivate bounce
			//Undo the momentum cut from bounce activation/deactivation
			pmo.momx = FixedMul(P_ReturnThrustX(nil,momangle,pmom),pmo.scale)
			pmo.momy = FixedMul(P_ReturnThrustY(nil,momangle,pmom),pmo.scale)
			pmo.momz = $*2
		end
		//Update bounce history
		player.bouncelast = (player.pflags&PF_BOUNCING > 0)
	end
end

local weight = function(player)
	if not(player.mo) then return end
	
	local w
	if S[player.skinvars].weight then
		w = S[player.skinvars].weight
	else
		w = S[-1].weight
	end
	
	player.mo.weight = FRACUNIT*w/100
end

local exhaust = function(player)
	if not (player and player.mo and player.mo.valid)
		return
	end
	local mo = player.mo
	
	//Do func_exhaust
	local defaultfunc = S[-1].func_exhaust
	local func = S[player.skinvars].func_exhaust
	local override = nil
	if not func
		func = defaultfunc
	end
	if func
		override = func(player)
	end
	
	//Common exhaust states
	local warningtic = FRACUNIT/3
	/*if player.pflags & PF_STARTDASH
		override = true
		local maxtime = 5*TICRATE
		player.exhaustmeter = max(0,$-FRACUNIT/maxtime)
		if player.exhaustmeter <= 0
			player.exhaustmeter = FRACUNIT
			player.pflags = $ & ~(PF_STARTDASH|PF_SPINNING)
			mo.state = S_PLAY_STND
		end
	end*/
	
	//Refill meter
	if (P_IsObjectOnGround(mo) or P_PlayerInPain(player)) and not player.actionstate and not override
		player.exhaustmeter = FRACUNIT
	end
	
	//Exhaust warning / color
	if mo.exhaustcolor == nil then
		mo.exhaustcolor = false
	end
	if mo.exhaustcolor == true and player.exhaustmeter == FRACUNIT
		mo.colorized = false
		mo.color = player.skincolor
		mo.exhaustcolor = false
	end
	local colorflip = false
	if player.exhaustmeter < warningtic and not(player.exhaustmeter == 0) then
		if not(leveltime&7)
			S_StartSound(mo,sfx_s3kbb,player)
		end
		if (leveltime&4)
			colorflip = true
		end
	end
	if colorflip == true
		mo.exhaustcolor = true
		mo.colorized = true
		mo.color = SKINCOLOR_BRONZE
	elseif mo.exhaustcolor == true
		mo.exhaustcolor = false
		mo.colorized = false
		mo.color = player.skincolor		
	end
end

local flashingnerf = function(player)
	if not B.BattleGametype() return end
	if player.powers[pw_flashing] < (3 * TICRATE - 1)
		player.powers[pw_flashing] = min($, 2*TICRATE)
	end
end

B.CharAbilityControl = function(player)
	B.ArmaCharge(player)
	homingthok(player)
	popgun(player)
	pogo(player)
	weight(player)
	exhaust(player)
	flashingnerf(player)
end

B.MidAirAbilityAllowed = function(player)
	if player.gotcrystal or player.gotflag then 
		return false
	else
		return true
	end
end

