freeslot(
	'spr_gbuz',
	's_buzzbuddy1',
	's_buzzbuddy2',
	'mt_buzzbuddy'
)

states[S_BUZZBUDDY1] = {
	sprite = SPR_GBUZ,
	frame = A,
	tics = 1,
	nextstate = S_BUZZBUDDY2
}
states[S_BUZZBUDDY2] = {
	sprite = SPR_GBUZ,
	frame = B,
	tics = 1,
	nextstate = S_BUZZBUDDY1
} --REMEMBER its NOT spawning the right thing, fix that
mobjinfo[MT_BUZZBUDDY] = {
	spawnstate = S_BUZZBUDDY1,
	deathstate = S_BUMBLEBORE_DIE,
	spawnhealth = 0,
	deathsound = sfx_pop,
	speed = 4*FRACUNIT,
	radius = 20*FRACUNIT,
	height = 24*FRACUNIT,
	mass = 0,
	damage = 0,
	flags = MF_ENEMY|MF_SPECIAL|MF_SHOOTABLE|MF_NOGRAVITY
}